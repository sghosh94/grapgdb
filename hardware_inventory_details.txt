
CREATE  CONSTRAINT ON (asset:ASSET) ASSERT asset.name IS UNIQUE;
CREATE  CONSTRAINT ON (domain:DOMAIN) ASSERT domain.name IS UNIQUE;
CREATE  CONSTRAINT ON (man:MANUFACTURER) ASSERT man.name IS UNIQUE;

CREATE  CONSTRAINT ON (loc:LOCATION) ASSERT loc.name IS UNIQUE;
CREATE  CONSTRAINT ON (w:WARRANTY_STATUS) ASSERT w.value IS UNIQUE;
CREATE  CONSTRAINT ON (os: OS) ASSERT os.name IS UNIQUE;
--------------------------------
 MATCH( src_ips:SRC_IP_ADDR)
 MATCH( dst_ips:DST_IP_ADDR)

 WHERE src_ips.name =line.IP_ADDRESS OR dst_ips.name= line.IP_ADDRESS
-----------------------------------------

:auto USING PERIODIC COMMIT 100

LOAD CSV WITH HEADERS FROM "https://gitlab.com/sghosh94/grapgdb/-/raw/0395250ba7780c9a74d1af4ce15885406b9520f0/hardware-inventory-dataset.csv" AS line WITH line



 MATCH( src_ip:SRC_IP_ADDR)
 MATCH( dst_ip:DST_IP_ADDR) 


 MERGE (asset:ASSET{name:line.ASSET_NAME})

 SET asset.description=line.DESCRIPTION
 SET asset.model=line.MODEL
 SET asset.internal_Storage = line.INTERNAL_STORAGE
 SET asset.status=line.ASSET_STATUS

 MERGE(domain:DOMAIN{name:line.DOMAIN})
 MERGE(man:MANUFACTURER{name:line.MANUFACTURER})
 MERGE(loc:LOCATION{name:line.LOCATION})
 MERGE(w:WARRANTY_STATUS{value:line.WARRANTY_STATUS})
 MERGE(os:OS{name:line.OS})

 MERGE (asset)-[:located_at]->(loc)
 MERGE (asset)-[:domain_aasociation]->(domain)
 MERGE (asset)-[:warranty_status]->(w)
 MERGE (asset)-[:os_type]->(os)
 
 MERGE (asset)-[:manufactured_by]->(man)
 MERGE (man)-[:manufactured_asset]->(asset)
 
case line.IP_ADDRESS
  when dst_ip.name then MERGE (asset)-[:associate_ip]->(dst_ip)

 			MERGE (dst_ip)-[:belongs_to]->(asset)
  when src_ip.name then MERGE (asset)-[:associate_ip]->(src_ip)
       
                        MERGE (src_ip)-[:belongs_to]->(asset)
 end;
 
 
;

